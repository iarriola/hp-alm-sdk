﻿using System;
using System.Windows.Forms;
using ALMLogin;
using Microsoft.Win32;
using System.IO;

namespace ALMLoginUI
{
    public partial class LoginForm : Form
    {
        public LoginForm(string[] args = null)//ITDConnection2 TDC = null)
        {
            InitializeComponent();
            ValidateInputParameters(args);
            ValidateALMAPIs();
            //if (TDC != null)
            //{
            //    this.TDC = TDC;
            //}
        }

        #region Properties

        string Msgtitle = "HP ALM Login";

        private ALMConnectionParams parameters;
        public ConnectionType CType = ConnectionType.OTA;

        private IALMConnection connection;
        public IALMConnection ALMConnection { get { return connection; } }

        private ALMConnectionOTA connectionOTA = null;
        private ALMConnectionSA connectionSA = null;
        private ALMConnectionREST connectionREST = null;

        string[] InputParameters;

        #endregion

        #region Events

        void connection_Disconnected(object sender, EventArgs e)
        {
            lblConnectionColor.BackgroundImage = Properties.Resources.red;
            lblConnection.Text = "Disconnected";

            cmbDomain.DataSource = null;
            cmbProject.DataSource = null;
            cmbProject.Items.Clear();
            cmbDomain.Items.Clear();

            txtServer.Enabled = true;
            txtPw.Enabled = true;
            txtUser.Enabled = true;
            btnAuth.Enabled = true;
            ckbSA.Enabled = true;
            cmbDomain.Enabled = false;
            cmbProject.Enabled = false;
            btnLogout.Enabled = false;
            btnLogin.Enabled = false;
            btnDisconnect.Enabled = false;

            txtServer.Clear();
            txtPw.Clear();
            txtUser.Clear();
            cmbDomain.Items.Clear();
            cmbProject.Items.Clear();
        }

        void connection_Connected(object sender, EventArgs e)
        {
            lblConnectionColor.BackgroundImage = Properties.Resources.green;
            lblConnection.Text = "Connected";

            btnLogout.Enabled = true;
            txtServer.Enabled = false;
            txtPw.Enabled = false;
            txtUser.Enabled = false;
            btnAuth.Enabled = false;
            cmbDomain.Enabled = true;
            ckbSA.Enabled = false;

            if (CType == ConnectionType.OTA)
            {
                FillDomains();    
            }

        }

        void connection_Authenticated(object sender, EventArgs e)
        {
            this.cmbDomain.Enabled = false;
            this.cmbProject.Enabled = false;
            this.btnLogin.Enabled = false;
            btnDisconnect.Enabled = true;

        }

        void connection_DisconnectedFromProject(object sender, EventArgs e)
        {
            ckbSA.Enabled = false;
            cmbDomain.Enabled = true;
            cmbProject.Enabled = true;
            btnLogout.Enabled = true;
            btnLogin.Enabled = true;
            btnDisconnect.Enabled = false;
        }


        #endregion

        #region Control Events

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUser.Text.Trim()))
            {
                btnAuth.Enabled = true;
            }
            else
            {
                btnAuth.Enabled = false;
            }
        }

        private void ckbSA_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ckbSA.Checked)
            {
                cmbDomain.Enabled = false;
                cmbProject.Enabled = false;
                btnLogin.Enabled = false;
                this.CType = ConnectionType.SA;
                
            }
            else
            {
                this.CType = ConnectionType.OTA;
            }

            ToggleForm();

        }

        private void btnAuth_Click(object sender, EventArgs e)
        {
            if (!MandatoryFields()) return;
        
            parameters = new ALMConnectionParams(txtServer.Text, txtUser.Text, txtPw.Text);
                
            connection = ALMConnectionFactory.GetALMConnection(this.CType, parameters);
            connection.Connected += connection_Connected;
            connection.Disconnected += connection_Disconnected;
            connection.DisconnectedFromProject += connection_DisconnectedFromProject;

            SetLocalConnections();

            try
            {
                connection.Connect();
            }
            catch (Exception ex)
            {
                ALMConnectionFactory.WipeALMConnection();
                MessageBox.Show(ex.Message, Msgtitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            connection.Authenticated += connection_Authenticated;

            if (connection.IsConnected())
            {
                connection.SetConnectionParams(parameters);

                try
                {
                    connection.Authenticate();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Msgtitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void cmbDomain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDomain.DataSource != null)
            {
                this.parameters.Domain = cmbDomain.SelectedValue.ToString();    
            }

            FillProjects();
        }

        private void cmbProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProject.SelectedIndex != -1 && cmbProject.SelectedValue.ToString() != string.Empty)
            {
                this.parameters.Project = cmbProject.SelectedValue.ToString();
                btnLogin.Enabled = true;
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            connection.Disconnect();
            ALMConnectionFactory.WipeALMConnection();
            LoadLastConnection();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            LoadLastConnection();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            connection.DisconnectProject();
        }

        #endregion

        #region Form Methods

        private void FillDomains()
        {
            cmbDomain.DataSource = this.connectionOTA.GetDomains();
            cmbDomain.DisplayMember = "Name";
            cmbDomain.ValueMember = "Value";

            FillProjects();
        }

        private void FillProjects()
        {
            if (cmbDomain.SelectedIndex != -1)
            {
                cmbProject.Enabled = true;
                cmbProject.DataSource = null;
                cmbProject.Items.Clear();
                cmbProject.DataSource = this.connectionOTA.GetProjects(cmbDomain.SelectedValue.ToString());
                cmbProject.DisplayMember = "Name";
                cmbProject.ValueMember = "Value";
            }            
        }

        private string GetLastServerConnection()
        {

            try
            {
                RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\Mercury Interactive\\TestDirector\\WEB", false);

                string keyValue = regKey.GetValue("LastConnection").ToString();

                if (keyValue.ToUpper().Contains("QCBIN"))
                {
                    keyValue = keyValue.Substring(0, keyValue.IndexOf("QCBIN", 0, StringComparison.CurrentCultureIgnoreCase) + 5);
                }
                else
                {
                    keyValue = keyValue.Substring(0, keyValue.IndexOf("SABIN", 0, StringComparison.CurrentCultureIgnoreCase)) + "qcbin";
                }

                return keyValue;
            }
            catch
            {
                return string.Empty;
            }

        }

        private void LoadLastConnection()
        {
            txtServer.Text = GetLastServerConnection();

            if (!string.IsNullOrEmpty(txtUser.Text))
            {
                btnAuth.Enabled = true;
                txtPw.Focus();
            }
            else
            {
                txtUser.Focus();
            }
        }

        private bool MandatoryFields()
        {

            if (string.IsNullOrEmpty(txtServer.Text.Trim()) | string.IsNullOrEmpty(txtUser.Text.Trim()))
            {
                MessageBox.Show("ALM Server and Login Name fields cannot be empty.", Msgtitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                if (!string.IsNullOrEmpty(txtServer.Text))
                {
                    txtUser.Focus();
                }
                else
                {
                    txtServer.Focus();
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        private void ToggleForm()
        {
            if (this.Height > 235)
            {
                for (int i = 340; i >= 235; i--)
                {
                    this.Height = i;
                    this.StartPosition = FormStartPosition.CenterScreen;
                }
            }
            else if (this.Height < 350)
            {
                for (int i = 235; i <= 350; i++)
                {
                    this.Height = i;
                    this.StartPosition = FormStartPosition.CenterScreen;
                }
            }

        }

        private void SetLocalConnections() 
        {
            if (connection != null)
            {
                switch (CType)
                {
                    case ConnectionType.OTA:
                        this.connectionOTA = ((ALMConnectionOTA)this.connection);
                        break;
                    case ConnectionType.SA:
                        this.connectionSA = ((ALMConnectionSA)this.connection);
                        break;
                    case ConnectionType.REST:
                        this.connectionREST = ((ALMConnectionREST)this.connection);
                        break;
                    default:
                        break;
                }
            }
        }

        public object GetConnection() 
        {
            try
            {
                if (connectionOTA != null)
                {
                    return (object)connectionOTA.GetTDConnection();
                }

                if (connectionSA != null)
                {
                    return (object)connectionSA.GetSAConnection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Msgtitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return null;

        }

        private void ValidateInputParameters(string[] inputArgs)
        {
            if (inputArgs != null)
            {

            }
        }

        private void ValidateALMAPIs()
        {
            string pathToALM = string.Format(@"{0}\HP\ALM-Client", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));

            if (!Directory.Exists(pathToALM))
            {
                if (MessageBox.Show("HP ALM Open Test Architecture (OTA) API it is not currently registered in the system. Wold you like to use the REST API?", Msgtitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    this.ckbSA.Checked = false;
                    this.CType = ConnectionType.REST;

                }
                else
                {
                    Application.Exit();
                }
            }

        }

        #endregion
    }
}