﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALMLogin
{
    public enum ConnectionType
    {
        SA,
        OTA,
        REST,
        NULL
    }

    public class ALMDataSet
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ALMConnectionFactory
    {
        private static IALMConnection ALMConnection = null;

        private ConnectionType CType = ConnectionType.NULL;

        public ALMConnectionParams CParams { get; set; }

        private ALMConnectionFactory()
        {
            
        }

        public static IALMConnection GetALMConnection(ConnectionType CType, ALMConnectionParams CParams)
        {
            if (ALMConnection == null)
            {

                switch (CType)
                {
                    case ConnectionType.SA:
                        {
                            ALMConnection = new ALMConnectionSA(CParams);
                            break;
                        }
                        
                    case ConnectionType.OTA:
                        {
                            ALMConnection = new ALMConnectionOTA(CParams);
                            break;    
                        }
                        
                    case ConnectionType.REST:
                        break;
                    default:
                        {
                            ALMConnection = ALMConnectionNull.Instance;
                            break;    
                        }
                        
                }

            }

            return ALMConnection;
        }

        public static void WipeALMConnection()
        {
            ALMConnection = null;
        }
    }
}