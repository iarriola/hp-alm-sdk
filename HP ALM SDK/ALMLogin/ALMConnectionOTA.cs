﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TDAPIOLELib;

namespace ALMLogin
{
    public class ALMConnectionOTA : IALMConnection
    {
        #region Events

        public event EventHandler Connected;
        public event EventHandler Disconnected;
        public event EventHandler Authenticated;
        public event EventHandler DisconnectedFromProject;

        #endregion

        #region Properties

        public EventArgs e = null;
        private Exception WorkerError;
        private ManualResetEvent WorkerCompletedEvent;
        private BackgroundWorker worker = new BackgroundWorker();

        private bool _IsConnected { get; set; }

        public ALMConnectionParams ConnectionString { get; set; }

        private TDConnection ALMConnection = null;

        #endregion

        #region Implementation

        public ALMConnectionOTA(ALMConnectionParams connectionParams)
        {
            this.ConnectionString = connectionParams;
        }

        public bool Connect()
        {
            try
            {
                ALMConnection = new TDConnection();
                ALMConnection.InitConnectionEx(ConnectionString.Server);

                worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                
                WorkerCompletedEvent = new System.Threading.ManualResetEvent(false);

                worker.RunWorkerAsync();

                WorkerCompletedEvent.WaitOne();

                if (WorkerError != null)
                {
                    throw WorkerError;   
                }
                else
                {
                    if (Connected != null)
                    {
                        Connected(this, this.e);
                    }
                }

                return ALMConnection.Connected;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Authenticate()
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.RunWorkerAsync();

                worker.DoWork += delegate(object s, DoWorkEventArgs args) {
                    ALMConnection.Connect(ConnectionString.Domain, ConnectionString.Project);
                    
                    args.Result = true;
                };

                worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {

                    if (Authenticated != null)
                    {
                        Authenticated(this, e);
                    }

                    ALMConnection.RefreshConnectionState();
                };

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Disconnect()
        {
            try
            {
                DisconnectProject();

                if (ALMConnection.LoggedIn)
                {
                    ALMConnection.Logout();
                }

                if (ALMConnection.Connected)
                {
                    ALMConnection.ReleaseConnection();
                }

                if (Disconnected != null)
                {
                    Disconnected(this, e);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DisconnectProject()
        {
            try
            {
                ALMConnection.RefreshConnectionState();

                if (ALMConnection.Connected)
                {
                    ALMConnection.Disconnect();
                }

                if (DisconnectedFromProject != null)
                {
                    DisconnectedFromProject(this, e);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsConnected()
        {
            return _IsConnected;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ALMConnection.Login(ConnectionString.User, ConnectionString.Password);
                e.Result = ALMConnection.Connected;
            }
            catch (Exception ex)
            {
                WorkerError = ex;
                e.Result = ex;
            }

            WorkerCompletedEvent.Set();
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                WorkerError = e.Error;
            }
            else
            {
                WorkerError = null;
            }

            this._IsConnected = ALMConnection.Connected;

            WorkerCompletedEvent.Set();

        }

        #endregion

        #region Class Methods

        public TDConnection GetTDConnection()
        {
            if (ALMConnection.Connected)
            {
                return ALMConnection;
            }
            else
            {
                return new TDConnection();
            }
            
        }

        public List<ALMDataSet> GetDomains()
        {
            List<ALMDataSet> list = new List<ALMDataSet>();

            try
            {
                foreach (var domain in ALMConnection.DomainsList)
                {
                    list.Add(new ALMDataSet() { Name = domain.ToString(), Value = domain.ToString() });
                }
            }
            catch (COMException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return list;
        }

        public List<ALMDataSet> GetProjects(string projectName)
        {
            List<ALMDataSet> list = new List<ALMDataSet>();

            try
            {
                foreach (var project in ALMConnection.ProjectsListEx[projectName])
                {
                    list.Add(new ALMDataSet() { Name = project.ToString(), Value = project.ToString() });
                }
            }
            catch (COMException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return list;
        }

        public void SetConnectionParams(ALMConnectionParams connectionParams)
        {
            this.ConnectionString = connectionParams;
        }

        #endregion
    }
}