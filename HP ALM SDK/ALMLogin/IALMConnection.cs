﻿using System;

namespace ALMLogin
{
    public interface IALMConnection
    {
        bool Authenticate();
        void Disconnect();
        void DisconnectProject();
        bool Connect();
        bool IsConnected();
        void SetConnectionParams(ALMConnectionParams connectionParams);
        event EventHandler Connected;
        event EventHandler Authenticated;
        event EventHandler Disconnected;
        event EventHandler DisconnectedFromProject;
    }
}