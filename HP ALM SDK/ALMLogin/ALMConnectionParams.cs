﻿using System;

namespace ALMLogin
{
    public class ALMConnectionParams
    {
        public ALMConnectionParams()
        {

        }

        public ALMConnectionParams(string server, string user, string pw)
        {
            Server = server;
            User = user;
            Password = pw;
        }

        public ALMConnectionParams(string server, string user, string pw, string domain, string project)
        {
            Server = server;
            User = user;
            Password = pw;
            Domain = domain;
            Project = project;
        }

        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string Project { get; set; }
        
    }
}
