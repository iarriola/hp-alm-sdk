﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SACLIENTLib;
using System.Runtime.InteropServices;

namespace ALMLogin
{
    public class ALMConnectionSA : IALMConnection
    {
        #region Events

        public event EventHandler Connected;
        public event EventHandler Authenticated;
        public event EventHandler Disconnected;
        public event EventHandler DisconnectedFromProject;

        #endregion

        #region Properties

        public EventArgs e = null;

        public ALMConnectionParams _ConnectionString { get; set; }

        private bool _IsConnected;

        private SAapi SAC = null;

        #endregion

        #region Implementation

        public ALMConnectionSA(ALMConnectionParams connectionParams)
        {
            this._ConnectionString = connectionParams;
        }

        public bool Connect()
        {
            try
            {
                this.SAC = new SAapi();
                this.SAC.Login(_ConnectionString.Server, _ConnectionString.User, _ConnectionString.Password);

                if (Connected != null)
                {
                    _IsConnected = true;
                    Connected(this, this.e);
                }

                return true;
            }
            catch (COMException ex)
            {               
                throw new COMException(FixExeceptionMessage(ex.Message), ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Authenticate()
        {
            throw new NotImplementedException();
        }

        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public bool IsConnected()
        {
            return _IsConnected;
        }

        public void SetConnectionParams(ALMConnectionParams connectionParams)
        {
            _ConnectionString = connectionParams;
        }

        #endregion

        #region Class Methods

        public SAapi GetSAConnection()
        {
            return this.SAC;
        }

        private string FixExeceptionMessage(string message)
        {
            string[] tem;

            tem = message.Split(';');

            tem = tem[0].Split(':');

            return tem[1];
        }

        public void DisconnectProject()
        {
            //do nothing...
        }

        #endregion
    }
}