﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALMLogin
{
    class ALMConnectionNull : IALMConnection
    {
        public bool Connect()
        {
            return false;
        }

        public bool Authenticate()
        {
            return false;
        }

        public void Disconnect()
        {
            //do nothing...
        }

        public void Dispose()
        {
            //do nothing...
        }

        public event EventHandler Connected;

        public event EventHandler Disconnected;

        private static IALMConnection instance = new ALMConnectionNull();

        public static IALMConnection Instance
        {
            get { return instance; }
        }


        public event EventHandler Authenticated;
        public event EventHandler DisconnectedFromProject;

        public bool IsConnected()
        {
            throw new NotImplementedException();
        }

        public void SetConnectionParams(ALMConnectionParams connectionParams)
        {
            //do nothing...
        }

        public void DisconnectProject()
        {
            //do nothing...
        }
    }
}
