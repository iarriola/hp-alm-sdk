﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALMLogin
{
    public class ALMConnectionREST : IALMConnection
    {
        public bool Authenticate()
        {
            throw new NotImplementedException();
        }

        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public bool Connect()
        {
            throw new NotImplementedException();
        }

        public bool IsConnected()
        {
            throw new NotImplementedException();
        }

        public void SetConnectionParams(ALMConnectionParams connectionParams)
        {
            throw new NotImplementedException();
        }

        public void DisconnectProject()
        {
            throw new NotImplementedException();
        }

        public event EventHandler Connected;

        public event EventHandler Authenticated;

        public event EventHandler Disconnected;
        public event EventHandler DisconnectedFromProject;
    }
}
