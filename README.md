# README #

HP ALM SDK for .NET developers

### What is this repository for? ###

* Quick summary
HP ALM SDK for .NET developers. Faster development for integrating third party application with HP Application Lifecycle Management. 
* Version
1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
1. RestSharp
2. SA and OTA client registered
* Deployment instructions

Include the SDK assemblies on your project (soon will be uploaded to NuGet) and then write down these lines:

            var alm_connection_object; 
            
            using (var form = new LoginForm())
            {
                var result = form.ShowDialog();

                alm_connection_object = form.GetConnection();

                //cast required depending of which hp alm connection are you using (SA, OTA, REST)
            }

            //in case of using OTA
            MessageBox.Show(((TDConnection)alm_connection_object).UserName, "ALM Login");


This code will launch a generic UI login for HP ALM, allowing you to connect to all the interfaces that HP ALM provide (OTA, SA, REST), enabling you to creating projects, users, queering project entities or create a basic integration with at your HP ALM server in a few steps.

![login.jpg](https://bitbucket.org/repo/LK8Kjo/images/1774935663-login.jpg)

Additionally you can skip the UI and implement the generic login at you backend application using WCF or Web API. 

### Who do I talk to? ###

* Repo owner or admin
@iarriola